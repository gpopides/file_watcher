package main

func CompareFilesLists(currentList, newList[]string) (bool, []string) {
	diffMap := make(map[string]bool)
	diffList := make([]string, 0)

	for _, value := range currentList {
		diffMap[value] = true
	}

	for _, value := range newList {
		if _, exists := diffMap[value]; !exists {
			diffList = append(diffList, value)
		}
	}

	/*
	return if the files have changed (changed files array length > 0)
	and the files  that have changed
	 */
	return len(diffList) > 0, diffList
}
