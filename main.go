package main

import (
	"fmt"
	"io/ioutil"
	"time"
)

type FileWatcher struct {
	pathToWatch string
	filesList []string
	filesCount int
}

func constructFileWatcher(ptw string) *FileWatcher {
	fw := new(FileWatcher)
	fw.pathToWatch = ptw

	fw.filesCount = countFiles(fw)
	fw.filesList = constructFilesList(fw)

	return fw
}

func constructFilesList(watcher *FileWatcher) []string {
		dirFiles , err := ioutil.ReadDir(watcher.pathToWatch)
		newFilesList := make([]string, 0)

		if err != nil {
			panic(err)
		}

		for _, f := range dirFiles {
			newFilesList = append(newFilesList, f.Name())
		}

		return newFilesList
}

func constructNewFilesList(watcher *FileWatcher)  {
	newFilesList := constructFilesList(watcher)

	if filesHaveChanged, diffList := CompareFilesLists(watcher.filesList, newFilesList);
	filesHaveChanged {
		// if the files have changed, the watchers files array
		// should get the new list

		watcher.filesList = newFilesList
		// TODO add post requests to api here.
		fmt.Printf("Files have changed.  New files are %v, Current list is %v\n", diffList, watcher.filesList)
	} else {
		fmt.Printf("Files have not changed\n")
	}
}

func countFiles(fileWatcher *FileWatcher) int {
		dirFiles , err := ioutil.ReadDir(fileWatcher.pathToWatch)

		if err != nil {
			panic(err)
		}

		filesCount := len(dirFiles)

	return filesCount
}

func startWatching(watcher *FileWatcher)  {
	for i := 0; i < 5; i++ {
		time.Sleep(3 * 1000 * time.Millisecond) // 3secs
		constructNewFilesList(watcher)
	}
}

func main() {
	fw := constructFileWatcher("/home/woops/tmpfiles/")

	startWatching(fw)

}
